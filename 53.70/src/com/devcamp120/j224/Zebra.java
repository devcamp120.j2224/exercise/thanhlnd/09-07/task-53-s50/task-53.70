package com.devcamp120.j224;

    public class Zebra extends Animal{
       private boolean is_wild ;

    public Zebra(int age , String gender , boolean is_wild){
        this.age = age;
        this.gender = gender ;
        this.is_wild = is_wild;
    } 
    
    @Override
    public String toString(){
        return "Duck {\"gender\" : " + this.gender+ ", age : " + age + " , is_wild : "+ this.is_wild + "" + "}";
    }


    @Override
    public void SoundAnimal() {
        System.out.println("Zebra sound");
    }
    @Override
    public void EatAnimal() {
        System.out.println("Zebra eat grass");
    }
}


