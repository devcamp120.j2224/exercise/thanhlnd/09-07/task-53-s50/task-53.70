package com.devcamp120.j224;

    public class Fish extends Animal{
       private int size ;
       private boolean canEat ; 

    public Fish (int age , String gender , int size , boolean canEat ){
        this.age = age ; 
        this.gender = gender ;
        this.size = size ;
        this.canEat = canEat ;
    }
    @Override
    public String toString(){
        return "Fish {\"gender\" : " + this.gender+ ", age : " + age + " , size : "+ this.size + " cm , " + "canEat : " + this.canEat + "" + " + }";
    }
    @Override
    public void SoundAnimal() {
        System.out.println("Fish sound");  
    }
    @Override
    public void EatAnimal() {
      System.out.println("Fish eat worm");
    }
}
